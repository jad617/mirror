# Personal Debian Repository

This repo allow me to:
* Automate building .deb packages from source for Ubuntu 18.04+
* Deploy a docker cluster to store and provide a public mirror
* Test the .deb packages and the docker deployment

### Prerequisites

```
A server with:
docker=18.09.2
docker-compose>=1.24.0
```

### Currently building .deb packages from the following projects:
* https://www.github.com/Airblader/i3
* https://github.com/jaagr/polybar.github

## Getting Started

This project deploys multiple Docker containers:
* lb01              --> HAproxy loadbalance for NGINX servers and acts as a reverse-proxy for letsencrypt queries
* watcher01         --> Monitors and restart HAproxy when the the haproxy.cfg config file is modified
* letsencrypt01     --> Provide automatic cert creation/renewal using Certbot
* webXX             --> NGINX web servers for the public repository
* repo01            --> Runs dpkg-scanpackages

##### Currently only webXX can be scalled


```
containers
├── lb01
│   ├── config
│   │   └── haproxy
│   │       └── haproxy.cfg
│   └── Dockerfile
├── letsencrypt01
│   ├── config
│   │   └── cron
│   │       └── letsencrypt.sh
│   └── Dockerfile
├── repo01
│   ├── config
│   │   └── cron
│   │       └── scanpackages
│   └── Dockerfile
├── watcher01
│   └── Dockerfile
└── web01
    ├── config
    │   └── nginx
    │       └── default
    └── Dockerfile

```


### Docker-compose config file:


```
version: '3'

services:
  repo01:
    build: containers/repo01
    volumes:
    - debs:/data/repo/debs
    restart: unless-stopped
  web01:
    build: containers/web01
    ports:
    - "81:80"
    volumes:
    - ./containers/web01/config/nginx/:/etc/nginx/sites-enabled/
    - debs:/var/www/html/debs/
    restart: unless-stopped
  lb01:
    build: containers/lb01
    ports:
    - "80:80"
    - "443:443"
    volumes:
    - ./containers/lb01/config/haproxy/:/usr/local/etc/haproxy/
    - certs:/certs/
    restart: unless-stopped
  letsencrypt01:
    build: containers/letsencrypt01
    ports:
    - "8080:80"
    volumes:
    - certs:/certs/
    restart: unless-stopped
  watcher01:
    build: containers/watcher01
    volumes:
    - certs:/certs/
    - /var/run/docker.sock:/var/run/docker.sock
    restart: unless-stopped

volumes:
  debs:
    driver: local
    driver_opts:
      type: none
      device: /data/debs
      o: bind
  certs:
```

### Pipeline flow

The pipline is devided in 4 stages:

+ Build stage:
  - Fetch the source files for each project (1 project per job)
  - Compile them and install in a specific directory
  - Package it using fpm 
+ Test stage:
  - Install the .deb packages
+ Staging stage:
  - Push the newly created .debs to the Docker host
  - Push the docker and docker-compose configs the Docker host
  - Build the Docker cluster using docker-compose
+ Pre_prod stage:
  - Spawn multiple Ubuntu instances for each versions starting from Ubuntu 18.04
  - Add the Repository in /etc/apt/sources.list.d/
  - Install the packages from it 


