#! /bin/bash

certbot certonly --standalone --preferred-challenges http -d docker01.staging.nodestack.ca --agree-tos -m jadasmar@hotmail.com --non-interactive --force-renewal

cat /etc/letsencrypt/live/docker01.staging.nodestack.ca/fullchain.pem /etc/letsencrypt/live/docker01.staging.nodestack.ca/privkey.pem > /certs/docker01.staging.nodestack.ca.pem
